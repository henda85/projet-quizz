# projet quizz

* Affichage des questions et choix de la réponse
* Enchaînement de plusieurs questions (sans changement de page)
* Décompte des points et affichage du score (en temps réel et à la fin)
* Responsive
 
 
 
#Compétences:
* J'ai utilisé des variables, conditions, tableaux, boucles et fonctions. Manipulation du DOM (querySelector / Events).
 
 
# Réalisation
* le thème de votre quizz: CSS.
 
une explication de ma méthodologie pour le code:
 
* Création d'un tableau d'objet(question, choix, index de la bonne réponse).
* Affichage de question et c'est choix de réponse
* Capturer la réponse de l'utilisateur(une fois qu'il valide)
* Si la réponse est bonne j'incrémente le score
* Le bouton suivant va afficher la question suivante...
* Une fois mon tableau de question est fini le résultat de quizz est affiché avec un message


# Maquette


![wireframe portfolio](public/image/wireframe1.png)
![wireframe portfolio](public/image/wireframe2.png)


**lien**
https://henda85.gitlab.io/projet-quizz

## Available Scripts

### npm start

Runs the app in the development mode.
Open http://localhost:8080 to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.

### npm run build

Builds a static copy of your site to the `build/` folder.
Your app is ready to be deployed!

**For the best production performance:** Add a build bundler plugin like [@snowpack/plugin-webpack](https://github.com/snowpackjs/snowpack/tree/main/plugins/plugin-webpack) or [snowpack-plugin-rollup-bundle](https://github.com/ParamagicDev/snowpack-plugin-rollup-bundle) to your `snowpack.config.json` config file.

### Q: What about Eject?

No eject needed! Snowpack guarantees zero lock-in, and CSA strives for the same.
