/*let tabQuest[{question,possibilté['','',''],bonRep:''}];*/

let tabQuest = [
  {
    question: 'Quelle est le rôle du CSS?',
    possibilité: [
      'Créer des pages web à l\'apparence soignée',
      'Rendre la page dinamique',
      'Contrôler  l\'affichage des variables js'
    ],
    bonRep: 0,
  },
  {
    question: 'comment cibler plusieurs éléments d\'un coup',
    possibilité: [
      'En les listant, séparés par une virgule',

      'En les listant, séparés par un point virgule',

      'En les listant, séparés par un tiret du 6'
    ],
    bonRep: 0,
  },
  {
    question: 'body h1 + p {color: yellow;} cette écriture signifie',
    possibilité: [
      'Cibler tout h1 et les p de body',
      'Cibler le p qui vient juste aprés h1 de body'
    ],
    bonRep: 1,
  },
  {
    question: 'Pour la mise en forme des éléments HTML',
    possibilité: [
      'On les repére uniquement par leur nom de balise',
      'C \'est déconseillée d\'utiliser les identifiants',
      'On les repére par la classe et ou la balise et ou l\'identifiant'
    ],
    bonRep: 2,
  },
  {
    question: 'Ecrire du css dans le document HTML',
    possibilité: [
      'C\'est fortement conseiller',

      'Ca se fait avec le mot clé \'link\'',

      'Déclarer grâce à l\'attribut style et affecte un seul element HTML'
    ],
    bonRep: 2,
  },
  {
    question: 'Pour définir la couleur d\'un élément texte sélectionné on utilise la propriété css',
    possibilité: [
      'color',
      'text-decoration',
      'font-style'
    ],
    bonRep: 0
  },
  {
    question: 'Pourquoi est-il d\'usage de sauter une ligne entre chaque déclaration ',
    possibilité: [
      'Pour rien du tout',
      'Pour que le code soit bien interprété par les navigateurs',
      'Pour rendre le code plus lisible et la maintenance plus facile'
    ],
    bonRep: 2
  },
  {
    question: 'Combien de règles peut contenir un fichier CSS ',
    possibilité: [
      'Il n\'y a pas de limite, on peut en créer autant que l\'on veut',
      'Si le poids du fichier n\'excède pas 35Ko, c\'est tout bon',
      '150'
    ],
    bonRep: 0
  },
  {
    question: "Quelle est la  bonne syntaxe pour changer la police d'un texte?",

    possibilité: [
      "font-name",
      "font-style",
      "font-family"
    ],
    bonRep: 2

  },
  {
    question: "<img id=\"imgprincipale\" src=\"chat.png\"> la reponse est:",

    possibilité: [
      ".imgprincipale { }",
      "#imgprincipale { }",
      "imgprincipale { }"
    ],
    bonRep: 1

  }
];

/**
 @type {HTMLElement}
 */
let labelQues = document.querySelector('#question');

/**
  @tgit@gitlab.com:henda85/project-quizz.gitype {HTMLElement}
 */
let formCheck = document.querySelector('.form-check');//la div de input et label
/**
  @type {HTMLElement}
 */
let valider = document.querySelector('#valider');
/**
  @type {HTMLElement}
 */
let btnSuivant = document.querySelector('#suivant');
/**
  @type {HTMLElement}
 */
let hScore = document.querySelector('#h3Score');
/**
  @type {HTMLElement}
 */
  let button = document.querySelector('.button');

let indiceQuest = 0;

let score = 0;


/**
 * une fonction qui affiche
 * une question suivie de ses possibilités de réponses 
 */

function affichQuest() {
  for (let reponse of tabQuest[indiceQuest].possibilité) {
    const divRep = document.createElement('div')
    
    const input = document.createElement('input');
    
    const label = document.createElement('label');

    labelQues.textContent = indiceQuest + 1 + ')' + ' ' + tabQuest[indiceQuest].question;
    divRep.classList.add('proRep');
    input.classList.add('inputClass');
    input.type = "radio";
    input.name = "test";
    label.textContent = reponse;

    formCheck.appendChild(divRep);
    divRep.appendChild(input);
    divRep.appendChild(label);

  }
  valider.disabled = false;
}

affichQuest();

btnSuivant.addEventListener('click', (e) => {
  e.preventDefault();
  if ((valider.disabled)) {
    formCheck.innerHTML = '';
    
    if (indiceQuest < tabQuest.length - 1) {
      indiceQuest++;
      affichQuest();
    }
    else {
      quizFini();
    }
  }
  else {
    alert('choisissez une reponse et valider!');
  }
}
)
valider.addEventListener('click', () => {
  let repUtil = document.querySelectorAll('.inputClass');
  recherBonRep(repUtil);
  hScore.textContent = score;
  valider.disabled = true;
})


/**
 * fonction qui compare la réponse de l'utilisateur
 * avec la bonne réponse et incrémente le score en cas de 
 * bonne réponse
 * @param {Array} reponse 
 */
function recherBonRep(reponse) {
  for (let i = 0; i < reponse.length; i++) {
    if (reponse[i].checked) {
      if (i === Number(tabQuest[indiceQuest].bonRep)) {
        score++;
        hScore.textContent = score;
      }
    }
  }
}
/**
 *  la fonction qui va afficher la derniere page
 */

function quizFini() {
  button.style.display = "none";
  labelQues.style.display = "none";
  if (score >= tabQuest.length / 2) {
    hScore.textContent = ' ' + score + "   pas mal!";
  }
  if (score === tabQuest.length) {
    hScore.textContent = ' ' + score + "  Vous êtes désormais expert du css!";
  }
  if (score < (tabQuest.length / 2)) {

    hScore.textContent = ' ' + score + " vous pouvez refaire le quiz et s'améliorer!";

  }
}
